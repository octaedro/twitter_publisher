# -*- coding: utf-8 -*-
class Concept:
    def __init__(self, data):
        self.id = int(data['id'])
        self.name = str(data['name'].encode("utf-8"))
        self.publish = int(data['publish'])
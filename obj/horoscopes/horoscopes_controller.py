from obj.horoscopes.api_horoscopes_connector import ApiHoroscopesConnector
from obj.sign import Sign
from obj.prediction import Prediction
from obj.concept import Concept
from obj.connection import Connection

class HoroscopesController:
    def __init__(self, account):
        self.con = Connection()
        self.con.openCursor()
        self.account = account
        self.api = ApiHoroscopesConnector()

    def getPrediction(self):
        sign = Sign(self.getNextSign())
        self.concept_to_pusblish = Concept(self.getNextConcept())
        return self.addSign(self.getPredictionFromAPI(sign), sign)

    def getNextSign(self):
        next_sign = self.con.getNextSignHoroscopes()
        if next_sign:
            self.con.setNextSignHoroscopes(next_sign["id"])
            return next_sign
        self.con.setAllSignsZeroHroscopes()
        return self.getNextSign()

    def getNextConcept(self):
        next_concept = self.con.getNextConceptHoroscopes()
        if next_concept:
            self.con.setNextConceptHoroscopes(next_concept["id"])
            return next_concept
        self.con.setAllConceptsZeroHroscopes()
        return self.getNextConcept()

    def getPredictionsFromtwitter(self):
        ret = False
        signs_list = self.getAllSigns()
        for one_sign in signs_list:
            self.account.key_word = "#" + one_sign.name + "&#" + self.concept_to_pusblish.name
            tweets_list = self.account.getTweetsFromTwitter()
            if len(tweets_list) > 0:
                ret = self.saveTweetsList(tweets_list, one_sign)
        return ret

    def getAllSigns(self):
        array_signs = []
        signs_list = self.con.getAllSigns()
        for one_sign in signs_list:
            array_signs.append(Sign(one_sign))
        return array_signs

    def saveTweetsList(self, tweets_list, one_sign):
        for one_tweet in tweets_list:
            prediction = Prediction(one_tweet.encode("utf-8"), self.concept_to_pusblish.id)
            return self.api.send_prediction_to_api(prediction.prepareToSave(one_sign.name, self.concept_to_pusblish.name))

    def getPredictionFromAPI(self, sign):
        prediction = self.api.getPrediction(sign.id, self.concept_to_pusblish.id)
        if prediction:
            return Prediction(prediction,self.concept_to_pusblish.id)
        if self.getPredictionsFromtwitter():
            return self.getPredictionFromAPI(sign)
        return False

    def addSign(self, prediction, sign):
        count_sign = len(sign.name)
        if prediction:
            if len(prediction.content) < (280 - count_sign):
                prediction.content = "#" + sign.name.upper() + ": " + prediction.content
                return prediction
        return False
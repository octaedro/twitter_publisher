# -*- coding: utf-8 -*-
import config as configuration
import requests
import json
import ast

class ApiHoroscopesConnector:
    def __init__(self):
        self.token = ""
        self.makeLogin()

    def save_predictions(self,predictions):
        if self.isLoggedIn():
            for one_prediction in predictions:
                self.send_prediction_to_api(one_prediction)
        return True

    def isLoggedIn(self):
        if self.token == "":
            return self.makeLogin()
        else:
            return True

    def makeLogin(self):
        try:
            url = configuration.base_url + configuration.login_url
            headers = {"Content-Type": "application/json; charset=utf-8", "Data-Type": "json"}
            data_login = json.dumps({"username": configuration.username, "password": configuration.password}, ensure_ascii=False)
            r = requests.post(url, data=data_login, auth=('user', 'pass'), headers=headers)
            data = ast.literal_eval(r.content)
            self.token = data["token"]
            if r.status_code == 200 or r.status_code == 201:
                print "Login -  success!"
            else:
                print "Error! -> " + r.reason

        except requests.exceptions.RequestException as e:
            raise e
        except Exception as e:
            raise e

        return True

    def getPrediction(self, id_sign, id_concept):
        try:
            url = (
                configuration.base_url + configuration.get_prediction_url +
                "?concept=" + str(id_concept) +
                "&sign=" + str(id_sign) +
                "&minlen=" + str(40) +
                "&maxlen=" + str(280)
            )
            headers = {"Content-Type": "application/json; charset=utf-8",
                       "Data-Type": "json",
                       "crossDomain": "True",
                       "Authorization": "JWT " + self.token}
            r = requests.get(url, headers=headers)
            if r.status_code == 200 or r.status_code == 201:
                array_prediction = json.loads(r.content)
                if len(array_prediction) > 0:
                    prediction = array_prediction[0]["content"]
                    print "Post got -  success! -> " + prediction.encode('utf-8')
                    return prediction.encode('utf-8')
                return False
            else:
                data = ast.literal_eval(r.content)
                try:
                    if not (data["detail"] is None):
                        print(r.status_code, data["detail"])
                except:
                    print(r.status_code, "No tiene detalle")

        except requests.exceptions.RequestException as e:
            raise e

    def send_prediction_to_api(self, prediction):
        if prediction:
            try:
                url = configuration.base_url + configuration.create_predictions_url
                headers = {"Content-Type": "application/json; charset=utf-8",
                           "Data-Type": "json",
                           "crossDomain": "True",
                           "Authorization": "JWT " + self.token}
                data_login = json.dumps({"concept": prediction.id_concept, "content": prediction.content})
                r = requests.post(url, data=data_login, headers=headers)
                if r.status_code == 200 or r.status_code == 201:
                    print "Post created -  success!"
                    return True
                else:
                    data = ast.literal_eval(r.content)
                    try:
                        if not (data["detail"] is None):
                            print(r.status_code, data["detail"])
                    except:
                        print(r.status_code, "No tiene detalle")

            except requests.exceptions.RequestException as e:
                raise e
        return False
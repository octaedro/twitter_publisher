# -*- coding: utf-8 -*-
class Tweet:
    def __init__(self, data):
        if data:
            self.id = int(data['id'])
            self.content = str(data['content'].encode("utf-8"))

    def isNotNull(self):
        return hasattr(self, 'content')

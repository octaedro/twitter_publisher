# -*- coding: utf-8 -*-
import MySQLdb
import config as configuration

class Connection:

    def __init__(self):
        self.db = MySQLdb.connect(host=configuration.dbhost,
                                  user=configuration.dbuser,
                                  passwd=configuration.dbpass,
                                  db=configuration.dbname,
                                  charset='utf8mb4')

    def openCursor(self):
        self.cur = self.db.cursor()

    def closeConnection(self):
        self.db.close()

    def getAllAccountsData(self):
        array_resp = []
        sentense = self.createSelectAllSentense()
        self.cur.execute(sentense)
        for row in self.cur.fetchall():
            if not row[1] == "none":
                array_resp.append({
                    "consumerkey": row[1], "consumersecret": row[2],"accesstoken": row[3], "accesstokensecret": row[4],
                    "account": row[5], "key_word": row[6],"table": row[11], "publish_min": row[12],
                    "new_friend_cada": row[13], "remove_friend_cada": row[14]
                })
        return array_resp


    def getNewTweet(self, table):
        self.cur.execute(self.createSelectGetNewTweetSentense(table))
        for row in self.cur.fetchall():
            if not row[1] == "none":
                return {"id": row[0], "content": row[1]}
        return False

    def updateTweetStatus(self, one_account):
        if one_account.tweet.isNotNull():
            table = one_account.table
            id_tweet = one_account.tweet.id
            if not id_tweet == -1:
                self.cur.execute(self.updateTweetPublishSentense(table, id_tweet))
                self.cur.execute(self.updateTweetDatePubSentense(table, id_tweet))
                self.db.commit()
                return True
        return False

    def saveTweetsList(self, table, tweets_list):
        for one_tweet in tweets_list:
            try:
                sql_sentense = self.insertTweetSentense(str(table), one_tweet.encode("utf-8"))
                print sql_sentense
                self.cur.execute(sql_sentense)
                self.db.commit()
            except Exception as e:
                print "Duplicate entry: " + str(e)

    def setNoMoreCurrencyNews(self):
        self.cur.execute(self.setNoMoreCurrencyNewsSentense())
        self.db.commit()

    def setNextSignHoroscopes(self,id_sign):
        self.cur.execute(self.setNextSignHoroscopesSentense(id_sign))
        self.db.commit()

    def setAllSignsZeroHroscopes(self):
        self.cur.execute(self.setAllSignsZeroHroscopesSentense())
        self.db.commit()

    def setNextConceptHoroscopes(self,id_concept):
        self.cur.execute(self.setNextConceptHoroscopesSentense(id_concept))
        self.db.commit()

    def setAllConceptsZeroHroscopes(self):
        self.cur.execute(self.setAllConceptsZeroHroscopesSentense())
        self.db.commit()

    def getNextSignHoroscopes(self):
        self.cur.execute(self.getNextSignHoroscopesSentense())
        for row in self.cur.fetchall():
            if not row[1] == "none":
                return {"id": row[0], "name": row[1], "publish": row[2]}
        return False

    def getAllSigns(self):
        signs_list = []
        self.cur.execute(self.getAllSignsSentense())
        for row in self.cur.fetchall():
            if not row[1] == "none":
                signs_list.append({"id": row[0], "name": row[1], "publish": row[2]})
            else:
                return False
        return signs_list

    def getNextConceptHoroscopes(self):
        self.cur.execute(self.getNextConceptHoroscopesSentense())
        for row in self.cur.fetchall():
            if not row[1] == "none":
                return {"id": row[0], "name": row[1], "publish": row[2]}
        return False

    def createTable(self, table_account):
        try:
            self.cur.execute(self.createTableSentense(table_account))
            self.db.commit()
            return True
        except Exception as e:
            print "Error creating tables" + e

    def createSignsTable(self):
        try:
            self.cur.execute(self.createSignsTableSentense())
            self.db.commit()
            return True
        except Exception as e:
            print "Error creating tables" + e

    def fillSignsTable(self):
        try:
            self.cur.execute(self.insertSignsTableSentense())
            self.db.commit()
        except Exception as e:
            print "Error insert signs table"
            print e

    def createConceptsTable(self):
        try:
            self.cur.execute(self.createConceptsTableSentense())
            self.db.commit()
            return True
        except Exception as e:
            print "Error creating tables" + e

    def fillConceptsTable(self):
        try:
            self.cur.execute(self.fillConceptsTableSentense())
            self.db.commit()
        except Exception as e:
            print "Error insert concepts table"
            print e

    def createTwitterAccountsTable(self):
        try:
            self.cur.execute(self.createTwitterAccountsTableSentense())
            self.db.commit()
            return True
        except Exception as e:
            print "Error creating table " + configuration.account_table
            return False

    def createSignsTableSentense(self):
        return (
            # "DROP TABLE IF EXISTS `py_horoscopes_signs`;" +
            "CREATE TABLE IF NOT EXISTS `py_horoscopes_signs` (" +
            "`id` int(11) NOT NULL AUTO_INCREMENT," +
            "`name` longtext NOT NULL," +
            "`publish` tinyint(1) NOT NULL," +
            "PRIMARY KEY (`id`)" +
            ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )

    def createConceptsTableSentense(self):
        return (
            # "DROP TABLE IF EXISTS `py_horoscopes_concepts`;" +
            "CREATE TABLE IF NOT EXISTS `py_horoscopes_concepts` (" +
            "`id` int(11) NOT NULL AUTO_INCREMENT," +
            "`name` longtext NOT NULL," +
            "`publish` tinyint(1) NOT NULL," +
            "PRIMARY KEY (`id`)" +
            ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        )

    def createTwitterAccountsTableSentense(self):
        return (
            "CREATE TABLE IF NOT EXISTS `%s` (" +
            "`id` int(11) NOT NULL AUTO_INCREMENT," +
            "`consumerkey` varchar(255) NOT NULL," +
            "`consumersecret` varchar(255) NOT NULL," +
            "`accesstoken` varchar(255) NOT NULL," +
            "`accesstokensecret` varchar(255) NOT NULL," +
            "`cuenta` varchar(255) NOT NULL," +
            "`palabra_clave` varchar(255) NOT NULL," +
            "`url_fb` varchar(255) NOT NULL," +
            "`fb_app_id` bigint(20) DEFAULT NULL," +
            "`fb_app_secret` varchar(200) DEFAULT NULL," +
            "`fb_token` varchar(200) DEFAULT NULL," +
            "`tabla` varchar(100) NOT NULL," +
            "`publicar_cada` int(11) NOT NULL DEFAULT '7'," +
            "`new_friend_cada` int(11) NOT NULL DEFAULT '7'," +
            "`remove_friend_cada` int(11) NOT NULL DEFAULT '7'," +
            "PRIMARY KEY (`id`)" +
            ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        ) % (configuration.account_table)

    def createTableSentense(self, table_account):
        return ("CREATE TABLE IF NOT EXISTS `%s` (" +
            "`id` int(11) NOT NULL AUTO_INCREMENT," +
            "`content` varchar(285) CHARACTER SET utf8mb4 NOT NULL," +
            "`created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,"+
            "`published_date` timestamp NULL DEFAULT NULL,"+
            "`published` int(1) DEFAULT '0'," +
            "PRIMARY KEY (`id`)," +
            "UNIQUE KEY `content` (`content`)" +
            ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;") % (table_account)

    def insertSignsTableSentense(self):
        return (
            "INSERT INTO `py_horoscopes_signs` (`id`, `name`, `publish`) VALUES " +
            "(1,'aries',1), (2,'tauro',1), (3,'geminis',1),(4,'cancer',1)," +
            "(5,'leo',1),(6,'virgo',1),(7,'libra',1),(8,'escorpio',0)," +
            "(9,'sagitario',0),(10,'capricornio',0),(11,'acuario',0),(12,'piscis',0);"
        ).encode("utf-8")

    def fillConceptsTableSentense(self):
        return (
            "INSERT INTO `py_horoscopes_concepts` (`id`, `name`, `publish`) VALUES " +
            "(1,'trabajo',1),(2,'amor',0),(3,'dinero',0),(4,'estudio',1),(5,'salud',1)," +
            "(6,'amigos',1),(7,'familia',0),(8,'sexo',1),(9,'hoy',1),(10,'atención',0)," +
            "(11,'deporte',0),(12,'infidelidad',0),(13,'pareja',0),(14,'relacion',1),(15,'respeto',0)," +
            "(16,'necesidad',0),(17,'ambición',0),(18,'corazón',1),(19,'sorpresa',0),(20,'año',0)," +
            "(21,'miedo',0),(22,'suerte',1),(23,'engaña',0),(24,'compañia',0),(25,'mañana',0);"
        )

    def createSelectGetNewTweetSentense(self, table):
        if not table == '':
            return 'SELECT * FROM `%s` WHERE `published` = 0 ORDER BY id ASC LIMIT 0,1;' % (table)
        return False

    def createSelectAllSentense(self):
        return 'SELECT * FROM `%s` WHERE 1;' % (configuration.account_table)

    def getAllSignsSentense(self):
        return 'SELECT * FROM `py_horoscopes_signs`;'

    def getNextSignHoroscopesSentense(self):
        return 'SELECT * FROM `py_horoscopes_signs` WHERE `publish` = 0 AND `id` <> 0 ORDER BY `id` ASC LIMIT 0,1;'

    def getNextConceptHoroscopesSentense(self):
        return 'SELECT * FROM `py_horoscopes_concepts` WHERE `publish` = 0 AND `id` <> 0 ORDER BY `id` ASC LIMIT 0,1'

    def updateTweetPublishSentense(self,table, id_tweet):
        return 'UPDATE `%s` SET `published` = 1 WHERE `id` = %s' % (str(table), str(id_tweet))

    def updateTweetDatePubSentense(self,table, id_tweet):
        return 'UPDATE `%s` SET `published_date` = NOW() WHERE `id` = %s' % (str(table), str(id_tweet))

    def setNextSignHoroscopesSentense(self, id_sign):
        return 'UPDATE `py_horoscopes_signs` SET `publish` = 1 WHERE `id` = %s' % (str(id_sign))

    def setAllSignsZeroHroscopesSentense(self):
        return 'UPDATE `py_horoscopes_signs` SET `publish` = 0;'

    def setNextConceptHoroscopesSentense(self, id_concept):
        return 'UPDATE `py_horoscopes_concepts` SET `publish` = 1 WHERE `id` = %s' % (str(id_concept))

    def setAllConceptsZeroHroscopesSentense(self):
        return 'UPDATE `py_horoscopes_concepts` SET `publish` = 0;'

    def setNoMoreCurrencyNewsSentense(self):
        return 'UPDATE `cuentas_twitter` SET `publicar_cada`=0 WHERE `tabla` = "py_twitter_bcnews";'

    def insertTweetSentense(self, table, one_tweet):
        return 'INSERT INTO `%s`(`content`) VALUES ("%s")' % (table, one_tweet)



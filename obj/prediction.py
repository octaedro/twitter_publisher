# -*- coding: utf-8 -*-
import re

class Prediction:
    def __init__(self, content, concept):
        self.content = content
        self.id_concept = int(concept)

    def prepareToSave(self, sign, concept):
        #Remove URLs
        self.removeURLs()

        #Replace signs
        pattern = re.compile(sign, re.IGNORECASE)
        self.content = pattern.sub("[signo]", self.content)

        #Uppercase concepts
        pattern_up = re.compile(concept, re.IGNORECASE)
        self.content = pattern_up.sub(concept.upper(), self.content)

        if self.isValid():
            return self
        return False

    def isValid(self):
        has_no_insults = not self.hasInsults()
        has_no_usernames = not self.hasUsernames()
        return has_no_insults and has_no_usernames

    def hasInsults(self):
        unaccepted_words_list = ['puta','pija','concha','conchuda','chota','putona','vagina','verga','berga']
        return any(word in self.content for word in unaccepted_words_list)

    def hasUsernames(self):
        return "@" in self.content

    def removeURLs(self):
        self.content = re.sub(
            r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))',
            '', self.content
        )

        self.content = re.sub(
            r'(?i)\b((?:http?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))',
            '', self.content
        )

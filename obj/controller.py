# -*- coding: utf-8 -*-
from obj.account import Account
from obj.connection import Connection
from obj.horoscopes.horoscopes_controller import HoroscopesController
import random

class Controller:
    def __init__(self):
        self.con = Connection()
        self.con.openCursor()
        self.account_list = self.getAccountList()

    def run_process(self):
        accounts_to_publish = self.getAccountsToPublish()
        self.publish_list(accounts_to_publish[0])
        self.makeNewFrends(accounts_to_publish[1])
        self.removeOldFriendByAccount(accounts_to_publish[2])
        self.con.closeConnection()
        return True

    def getAccountList(self):
        accounts_list = []
        account_data = self.con.getAllAccountsData()
        for one_ad in account_data:
            accounts_list.append(Account(one_ad))
        return accounts_list

    def getAccountsToPublish(self):
        accounts_to_publish = []
        for one_account in self.account_list:
            if one_account.publish_min or one_account.new_friend_cada or one_account.remove_friend_cada:
                random_number_list = [self.getRandomNumberToPublish(one_account.publish_min),
                                      self.getRandomNumberToPublish(one_account.new_friend_cada),
                                      self.getRandomNumberToPublish(one_account.remove_friend_cada)]
                accounts_to_publish.append(self.addToAccountsToPublishByResult(random_number_list, one_account))
        return self.reorderTheListToPublish(accounts_to_publish)

    def reorderTheListToPublish(self, list):
        publish = []
        add_friend = []
        old_friend = []
        accounts_to_publish = [publish, add_friend, old_friend]
        for element in list:
            count = 0
            for opt in element:
                if opt:
                    # accounts_to_publish[count] = element[count]
                    accounts_to_publish[count].append(opt)
                count += 1
                if count == 3:
                    count = 0
        return accounts_to_publish

    def getRandomNumberToPublish(self, limit):
        ret = None
        if limit:
            ret = random.randint(1, limit)
        return ret

    def addToAccountsToPublishByResult(self, random_number_list, one_account):
        list = []
        for random_number in random_number_list:
            if random_number == 1:
                list.append(one_account)
            else:
                list.append(None)
        return list


    def publish_list(self,accounts_list):
        for one_account in accounts_list:
            if one_account.account == "mihoroscopoxdia":
                horoscopes_controller = HoroscopesController(one_account)
                prediction = horoscopes_controller.getPrediction()
                if not prediction:
                    return False
                one_account.fillTweet(prediction)
            else:
                one_account.getNewTweet(self.con)
                if one_account.tweet.isNotNull():
                    self.con.updateTweetStatus(one_account)

            if one_account.tweet.isNotNull():
                one_account.publish()

    def makeNewFrends(self,accounts_list):
        for one_account in accounts_list:
            one_account.setCurrentFriendsList()
            one_account.addNewFriend(one_account.getNewFrendFromTwitter())

    def removeOldFriendByAccount(self,accounts_list):
        for one_account in accounts_list:
            one_account.removeOldFriend(one_account.getAnyFriend())

    # DEPLOY CODE
    def deploy(self):
        for one_account in self.account_list:
            table = str(one_account.table)
            if not table == "":
                self.con.createTable(table)
            if one_account.account == "mihoroscopoxdia":
                if self.createHoroscopesSpecificTables():
                    self.fillTablesSignsAndConcepts()

    def createHoroscopesSpecificTables(self):
        return self.con.createSignsTable() and self.con.createConceptsTable()

    def fillTablesSignsAndConcepts(self):
        signs_bool = self.con.fillSignsTable()
        concepts = self.con.fillConceptsTable()

# -*- coding: utf-8 -*-
import tweepy
from obj.tweet import Tweet
import re
import random

class Account:
    def __init__(self,data):
        self.consumerkey = data["consumerkey"]
        self.consumersecret = data["consumersecret"]
        self.accesstoken = data["accesstoken"]
        self.accesstokensecret = data["accesstokensecret"]
        self.account = data["account"]
        self.key_word = "#" + str(data["key_word"])
        self.table = data["table"]
        self.publish_min = data["publish_min"]
        self.new_friend_cada = data["new_friend_cada"]
        self.remove_friend_cada = data["remove_friend_cada"]
        self.api = self.getAPI()
        self.friends_list = None

    def fillTweet(self, prediction):
        self.tweet = Tweet({"id": -1, "content": prediction.content})

    def getNewTweet(self,connection):
        self.tweet = Tweet(connection.getNewTweet(self.table))
        if self.tweet.isNotNull():
            return self.tweet
        if self.account == "currency_z":
            connection.setNoMoreCurrencyNews()
            return False
        tweets_list = self.getTweetsFromTwitter()
        if len(tweets_list) > 0:
            connection.saveTweetsList(self.table, self.validateList(tweets_list))
            return connection.getNewTweet(self.table)
        return False

    def publish(self):
        if self.tweet.isNotNull():
            print "publicando -> " + self.tweet.content
            # send message to twitter'
            # self.logTokenData()
            return self.api.update_status(self.tweet.content)

    def getTweetsFromTwitter(self):
        list_tweets = []
        search_number = 100
        search_result = self.api.search(q=self.key_word, lang="es", rpp=search_number)
        for i in search_result:
            try:
                if i.retweeted_status is None:
                    print "Is retweet"
            except:
                if not i.text == "":
                    if i.entities.get("urls"):
                        urls = i.entities.get("urls")[0]
                        txt = i.text.replace(urls.get("url"), urls.get("expanded_url"))
                    if "twitter.com" not in urls.get("display_url"):
                        list_tweets.append(txt)
        return list_tweets

    def getNewFrendFromTwitter(self):
        search_number = 100
        search_result = self.api.search(q=self.key_word, lang="es", rpp=search_number)
        for i in search_result:
            try:
                usefull = True
                if i.user.id:
                    for a_fiend in self.friends_list:
                        if a_fiend == i.user.id:
                            usefull = False
                    if usefull and i.user.screen_name != self.account:
                        return i.user.id
            except:
                print "Error getting new friend"
        return False

    def addNewFriend(self,friend_id):
        try:
            self.api.create_friendship(friend_id)
        except Exception as e:
            print "Error adding new friend"

    def removeOldFriend(self,friend_id):
        try:
            self.api.destroy_friendship(friend_id)
        except Exception as e:
            print "Error removing new friend"

    def getAnyFriend(self):
        if self.friends_list is None:
                self.setCurrentFriendsList()
        return self.friends_list[random.randint(0, len(self.friends_list)-3)]

    def setCurrentFriendsList(self):
        self.friends_list = self.api.friends_ids(self.account)

    def getAPI(self):
        # self.logTokenData()
        auth = tweepy.OAuthHandler(self.consumerkey, self.consumersecret)
        auth.set_access_token(self.accesstoken, self.accesstokensecret)
        return tweepy.API(auth)

    def validateList(self, list_tweets):
        array_clean = []
        for one_tweet in list_tweets:
            one_tweet = one_tweet.encode("utf-8")
            if not self.hasUsernames(one_tweet):
                array_clean.append(one_tweet)
                # array_clean.append(self.removeURLs(one_tweet))
        return array_clean

    def hasUsernames(self, tweet):
        return "@" in tweet

    def removeURLs(self, tweet):
        tweet = re.sub(
            r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))',
            '', tweet
        )

        tweet = re.sub(
            r'(?i)\b((?:http?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))',
            '', tweet
        )
        return tweet

    def logTokenData(self):
        print "-----------------------"
        print "account:" + self.account
        print "consumerkey:" + self.consumerkey
        print "consumersecret:" + self.consumersecret
        print "accesstoken:" + self.accesstoken
        print "accesstokensecret:" + self.accesstokensecret
        print "-----------------------"
